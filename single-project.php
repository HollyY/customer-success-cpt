<?php

get_header(); ?>

<section class="content">
  <div style="padding: 20px 0 0 0; font-size: 14px; background: #F9F9FB;">
<ul>
<li style="list-style: none; display: inline-block; font-weight: bold;"><a href="/customer-success/">All Customers</a> <span style="margin-left: 5px;">/</span></li>
<li style="list-style: none; display: inline-block; color: #999; margin-left: 5px;"><?php single_post_title(); ?></li>
</ul>
</div>
  <div class="customer-win">
    <?php

$image = get_field('main_logo');

if( !empty($image) ): ?>

	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

<?php endif; ?>

    <h1><?php the_field('main_headline'); ?></h1>
  </div>
  <hr>
  <section class="columns">
<article class="col col34">
<?php the_content(); ?>
<?php

$conclusion = get_field('conclusion_text');

if( !empty($conclusion) ): ?>
<hr>
<p><?php the_field('conclusion_text'); ?></p>
<?php endif; ?>
</article>
<article class="col col4">
<h5 class="subitem">Delivered by</h5><p>
<?php

$image = get_field('delivered');

if( !empty($image) ): ?>

<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

<?php endif; ?></p>

<div style="background: #EEEEEE; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; padding: 20px;">
<h5 class="subitem">Industry</h5>
<ul class="custom">
<li><i class="fa fa-check"></i><?php the_field('industry'); ?></li>
</ul>
<h5 class="subitem">Salesforce Licenses</h5>

<?php
// vars
$licenses = get_field('salesforce_licenses');

// check
if( $licenses ): ?>
<ul class="custom">
  <?php foreach( $licenses as $license ): ?>
  		<li><i class="fa fa-check"></i><?php echo $license; ?></li>
  	<?php endforeach; ?>
  </ul>
  <?php endif; ?>

<h5 class="subitem">Operating Systems</h5>
<?php
// vars
$systems = get_field('operating_systems');

// check
if( $systems ): ?>
<ul class="custom">
  <?php foreach( $systems as $system ): ?>
  		<li><i class="fa fa-check"></i><?php echo $system; ?></li>
  	<?php endforeach; ?>
  </ul>
  <?php endif; ?>

<h5 class="subitem">Number of Users</h5>
<ul class="custom">
<li><i class="fa fa-check"></i><?php the_field('number_of_users'); ?></li>
</ul>
</div></article>
</section>
</section>

<?php get_footer(); ?>
