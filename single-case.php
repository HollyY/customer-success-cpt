<?php

get_header(); ?>

<section class="content">
  <div class="case-study-content">

    <h2>Hi this is some text</h2>

    <?php
    $image = get_field('main_logo');
    if( !empty($image) ): ?>
    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
  <?php endif; ?>

  <h1><?php the_field('main_headline'); ?></h1>

</div>

<hr>

<section class="columns">
  <article class="col col34">
    <?php the_content(); ?>

    <?php

    $conclusion = get_field('conclusion_text');
    if( !empty($conclusion) ): ?>

    <hr>

    <p><?php the_field('conclusion_text'); ?></p>
  <?php endif; ?>

</article>

<article class="col col4">
  <h5 class="subitem">Delivered by</h5>

  <?php
  $image = get_field('delivered');
  if( !empty($image) ): ?>
  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
<?php endif; ?>

<div class="grey">
<h5 class="subitem">Industry</h5>
<ul class="custom">
<li><i class="fa fa-check"></i><?php the_field('industry'); ?></li>
</ul>
<h5 class="subitem">Salesforce Licenses</h5>

<?php
// vars
$licenses = get_field('salesforce_licenses');

// check
if( $licenses ): ?>
<ul class="custom">
  <?php foreach( $licenses as $license ): ?>
  		<li><i class="fa fa-check"></i><?php echo $license; ?></li>
  	<?php endforeach; ?>
  </ul>
  <?php endif; ?>

<h5 class="subitem">Operating Systems</h5>
<?php
// vars
$systems = get_field('operating_systems');

// check
if( $systems ): ?>
<ul class="custom">
  <?php foreach( $systems as $system ): ?>
  		<li><i class="fa fa-check"></i><?php echo $system; ?></li>
  	<?php endforeach; ?>
  </ul>
  <?php endif; ?>

<h5 class="subitem">Number of Users</h5>
<ul class="custom">
<li><i class="fa fa-check"></i><?php the_field('number_of_users'); ?></li>
</ul>
</div></article>
</section>
</section>

<?php get_footer(); ?>
